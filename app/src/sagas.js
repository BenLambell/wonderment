import { fork, call, put, takeEvery, takeLatest } from 'redux-saga/effects'
import startup from './sagas/startup'

function* sagas() {
    //yield takeEvery("USER_FETCH_REQUESTED", fetchUser);
    yield fork(startup);
}

export default sagas;