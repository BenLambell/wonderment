import React from 'react';
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import test from './reducers/test'
import App from './App';
import sagas from './sagas'
import Sockette from "sockette";

const sockette = new Sockette(
    "wss://gqcna0epc6.execute-api.ap-southeast-2.amazonaws.com/dev",
    {
        timeout: 5e3,
        maxAttempts: 1,
        onopen: e => console.log("connected:", e),
        onmessage: e => console.log("Message Received:", e),
        onreconnect: e => console.log("Reconnecting...", e),
        onmaximum: e => console.log("Stop Attempting!", e),
        onclose: e => console.log("Closed!", e),
        onerror: e => console.log("Error:", e)
    }
);

const sagaMiddleware = createSagaMiddleware()
const store = createStore(
    test,
    applyMiddleware(sagaMiddleware)
)

sagaMiddleware.run(sagas)

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)