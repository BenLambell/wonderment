from json import loads
from jsonschema import validate

point = {
    "type": "array",
    "minItems": 2,
    "maxItems": 2,
    "items": { "type": "integer" },
}

move = {
    "type" : "object",
    "properties" : {
        "start": point,
        "end": point,
    },
}

def parse(json: str, schema):
    result = loads(json)
    validate(result, schema)
    return result