import json
import logging
import boto3
import middleware
import responses
import schemas
import model

logger = logging.getLogger("handler_logger")
logger.setLevel(logging.DEBUG)
#logger.info("ping endpoint called.")

dynamodb = boto3.resource("dynamodb")

def getConnectionId(event):
    return event["requestContext"].get("connectionId")

def setConnection(topicId: int, connectionId, isConnected: bool):
    action = "ADD" if isConnected else "DELETE"
    connections = dynamodb.Table("wondermentConnections")
    connections.update_item(
        Key = { "topicId": topicId },
        ExpressionAttributeValues = { ":connectionId": { connectionId } },
        UpdateExpression = action + " connectionIds :connectionId")

def sendMessageToTopic(topicId, message, event):
    connections = dynamodb.Table("wondermentConnections")
    response = connections.query(
        KeyConditionExpression = "topicId = :topicId",
        ExpressionAttributeValues = { ":topicId" : topicId })
    connectionIds = [int(i) for i in response.get("Items", [])[0]["connectionIds"]]
    for connectionId in connectionIds:
        sendMessageToConnection(connectionId, message, event)

def sendMessageToConnection(connectionId, message, event):
    domainName = event["requestContext"]["domainName"]
    stage = event["requestContext"]["stage"]
    gatewayapi = boto3.client(
        "apigatewaymanagementapi",
        endpoint_url = "https://" + domainName + "/" + stage)
    return gatewayapi.post_to_connection(
        ConnectionId = connectionId,
        Data = json.dumps(message).encode('utf-8'))

@middleware.default
def connection_manager(event, context):
    eventType = event["requestContext"]["eventType"]
    if eventType == "CONNECT":
        return responses.success
    elif eventType == "DISCONNECT":
        return responses.success
    else:
        return responses.create(400, "Unrecognised event type.")
        
@middleware.default
def default_message(event, context):
    return responses.create(400, "Unrecognized WebSocket action.")

@middleware.default
def start(event, context):
    connectionId = getConnectionId(event)
    position = schemas.parse(event.body, schemas.point)
    viewingWindow = model.ViewingWindow(position[0], position[1])
    for addedTopicId in [model.getSectorHash(sector) for sector in viewingWindow.requiredSectors]:
        setConnection(addedTopicId, connectionId, True)
    return responses.success

@middleware.default
def move(event, context):
    connectionId = getConnectionId(event)
    move = schemas.parse(event.body, schemas.move)
    viewingWindowEvent = model.ViewingWindowEvent(move.start, move.end)
    for addedTopicId in [model.getSectorHash(sector) for sector in viewingWindowEvent.addedSectors]:
        setConnection(addedTopicId, connectionId, True)
    for removedTopicId in [model.getSectorHash(sector) for sector in viewingWindowEvent.removedSectors]:
        setConnection(removedTopicId, connectionId, False)
    return responses.success