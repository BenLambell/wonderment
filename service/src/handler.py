import json
import logging
import boto3
import middleware
import responses
import schemas
import model

logger = logging.getLogger("handler_logger")
logger.setLevel(logging.DEBUG)
#logger.info("ping endpoint called.")

dynamodb = boto3.resource("dynamodb")

@middleware.default
def ping(event, context):
    return responses.success

@middleware.default
def healthcheck(event, context):
    connections = dynamodb.Table("wondermentConnections")
    return responses.success