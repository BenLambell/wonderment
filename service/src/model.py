import numpy
import pymmh3 as mmh3
from typing import Tuple

sectorSize = 30
viewSize = 20
halfSectorSize = sectorSize / 2

def getSectorHash(point):
    return mmh3.hash(str(point), 0x42175dca)

class Sector:
    tiles = [[0] * sectorSize for i in range(sectorSize)]

class Tile:
    f = None
    w = None
    msg = None

class ObservationEvent:
    addedTopics = []
    removedTopics = []

__requiredOffsets__ = [(x, y) for y in range(0, 2) for x in range(0, 2)]
__allowedOffsets__ = [(x, y) for y in range(-1, 2) for x in range(-1, 2)]

class ViewingWindow:
    requiredSectors: set
    allowedSectors: set
    def __init__(self, positionX: int, positionY: int):
        position = numpy.array((positionX, positionY))
        sector = position // sectorSize
        sectorPosition = position % sectorSize
        windowOffset = numpy.where(sectorPosition > (sectorSize / 2), 1, -1)
        self.requiredSectors = set(tuple(i) for i in sector + windowOffset * __requiredOffsets__)
        self.allowedSectors = set(tuple(i) for i in sector + __allowedOffsets__)

class ViewingWindowEvent:
    addedSectors: set
    removedSectors: set
    def __init__(self, start: Tuple[int, int], end: Tuple[int, int]):
        self.start = start
        self.end = end
        startWindow = ViewingWindow(start)
        endWindow = ViewingWindow(end)
        self.addedSectors = startWindow.requiredSectors.difference(endWindow.requiredSectors)
        self.removedSectors = endWindow.allowedSectors.difference(startWindow.allowedSectors)