import json

def default(endpoint):
    def result(event, context):
        try:
            response = endpoint(event, context)
            return response
        except Exception as e:
            response = { "statusCode": 500, "body": "An unexpected exception occurred.\n\n" + repr(e) }
            return response
    return result