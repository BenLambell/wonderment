def create(statusCode: int, body: str):
    return { "statusCode": statusCode, "body": body }

success = create(200, "Success.")